# SearchFileShell

实现指定路径下指定字符文件的个数统计

## Installation
系统使用环境：linux
```
$git clone https://gitlab.com/cishidai/searchfileshell.git
$cd searchfileshell
```

## Usage
```
$./searchfile 指定文件路径 指定搜索文件名
```
举例
```
$./searchfile test _gt.json
```
说明
在test文件夹路径内统计*_gt.json*文件个数
